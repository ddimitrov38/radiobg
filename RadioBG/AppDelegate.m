//
//  AppDelegate.m
//  RadioBG
//
//  Created by Dimitar Dimitrov on 3/11/15.
//  Copyright (c) 2015 MMmedia. All rights reserved.
//

#import "AppDelegate.h"

// RadioInfo

@interface RadioInfo : NSObject
@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSURL* url;
@property (nonatomic, retain) NSString* icon;
@end

@implementation RadioInfo
@end


// AppDelegate

@interface AppDelegate() {
	NSMenu* volumeSubMenu;
	NSMenuItem *stopItem;
	NSMenu* radioSubMenu;

	NSInteger currentRadioIndex;
	float currentVolume;
}
@end

@implementation AppDelegate

static NSMutableArray* radioInfos;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];

	// Initialize from plist
	NSString *path = [[NSBundle mainBundle] pathForResource:@"radios" ofType:@"plist"];
	NSArray* array = [[NSMutableArray alloc]initWithContentsOfFile:path];
	radioInfos = [NSMutableArray array];

	for (int i = 0; i < array.count; i++) {
		RadioInfo* radioInfo = [[RadioInfo alloc] init];

		radioInfo.name = [[array objectAtIndex:i] objectForKey:@"name"];
		radioInfo.url = [NSURL URLWithString:[[array objectAtIndex:i] objectForKey:@"url"]];
//		radioInfo.icon = [NSString stringWithFormat:@"%@/%@", @"radio_icons", [[array objectAtIndex:i] objectForKey:@"icon"] ];
		radioInfo.icon = [[array objectAtIndex:i] objectForKey:@"icon"];

		[radioInfos addObject:radioInfo];
	}


	[self initMenu];

	[self.statusItem setMenu:self.statusMenu];
	[self.statusItem setToolTip:@"BG Radio"];
	[self.statusItem setImage:[NSImage imageNamed:@"normal"]];
	[self.statusItem setHighlightMode:YES];

	progressUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:3.0
														   target:self
														 selector:@selector(updateProgress:)
														 userInfo:nil
														  repeats:YES];

	currentVolume = 1.0f;
	currentRadioIndex = 0;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
}


- (void)initMenu {
	self.statusMenu = [[NSMenu alloc] initWithTitle:@"MainMenu"];
	[self.statusMenu setAutoenablesItems:NO];

	radioSubMenu = [[NSMenu alloc] initWithTitle:@"Select Radio"];
	NSMenuItem* radioListItem = [[NSMenuItem alloc] initWithTitle:@"Select Radio" action:nil keyEquivalent:@""];
	[self.statusMenu addItem:radioListItem];
	[self.statusMenu setSubmenu:radioSubMenu forItem:radioListItem];

	// Init radio menus
	for (int i = 0; i < radioInfos.count; i++) {
		RadioInfo* radioInfo = [radioInfos objectAtIndex:i];

		NSLog(@"%@", radioInfo.icon);

		NSMenuItem *radioItem = [[NSMenuItem alloc] initWithTitle:radioInfo.name action:@selector(selectedItem:) keyEquivalent:@""];
		[radioItem setImage: [NSImage imageNamed:radioInfo.icon]];
		[radioItem setTag:i];
		[radioSubMenu addItem:radioItem];
	}

	// Init main menus
	stopItem = [[NSMenuItem alloc] initWithTitle:@"Stop" action:@selector(stop) keyEquivalent:@""];
	[self.statusMenu addItem:stopItem];
	[stopItem setEnabled:NO];

	volumeSubMenu = [[NSMenu alloc] initWithTitle:@"Volume"];
	NSMenuItem* volumeMenuItem = [[NSMenuItem alloc] initWithTitle:@"Volume" action:nil keyEquivalent:@""];
	[self.statusMenu addItem:volumeMenuItem];
	[self.statusMenu setSubmenu:volumeSubMenu forItem:volumeMenuItem];

	// Init volume submenu
	for (int i = 100; i >= 0; i -= 25) {
		NSMenuItem* volumeItem = [[NSMenuItem alloc] initWithTitle:[NSString stringWithFormat:@"%d", i]
															action:@selector(changeVolume:) keyEquivalent:@""];
		volumeItem.tag = i;
		volumeItem.state = i == 100?NSOnState:NSOffState;
		[volumeSubMenu addItem:volumeItem];
	}

	NSMenuItem *aboutItem = [[NSMenuItem alloc] initWithTitle:@"About" action:@selector(about) keyEquivalent:@""];
	[self.statusMenu addItem:aboutItem];


	NSMenuItem *exitItem = [[NSMenuItem alloc] initWithTitle:@"Exit" action:@selector(exit) keyEquivalent:@"X"];
	[self.statusMenu addItem:exitItem];

}


#pragma mark Menu callbacks
-(void) selectedItem:(NSMenuItem*) item {
	RadioInfo* radioInfo = [radioInfos objectAtIndex:item.tag];
	currentRadioIndex = item.tag;

	NSLog(@"Switching to Radio: %@", radioInfo.name);

	for (NSMenuItem* i in radioSubMenu.itemArray)
		i.state = NSOffState; // Disable all

	item.state = NSOnState;

	[self destroyStreamer];

	[self.statusItem setToolTip:radioInfo.name];

	self.audioStreamer = [[AudioStreamer alloc] init];
	self.audioStreamer = [AudioStreamer streamWithURL:radioInfo.url];
	[self.audioStreamer setDelegate:self];
	[self.audioStreamer start];
}

-(void) exit {
	[progressUpdateTimer invalidate];
	progressUpdateTimer = nil;

	[NSApp terminate:self];
}

-(void) stop {
	[self destroyStreamer];
	NSMenuItem* currentRadioItem = [radioSubMenu.itemArray objectAtIndex:currentRadioIndex];
	currentRadioItem.state = NSMixedState;
}

-(void) about {
	[NSApp orderFrontStandardAboutPanel:self];
	[NSApp activateIgnoringOtherApps:YES];
}

-(void) changeVolume:(NSMenuItem *) item {
	if (self.audioStreamer) {
		currentVolume = (float) item.tag / 100;
		[self.audioStreamer setVolume:currentVolume];
	}

	for (NSMenuItem* i in volumeSubMenu.itemArray)
			i.state = NSOffState; // Disable all

	item.state = NSOnState;
}

-(void) destroyStreamer {
	[stopItem setEnabled:NO];

	if (self.audioStreamer) {
		[self.audioStreamer stop];
		self.audioStreamer = nil;
	}
}

#pragma mark AudioStreamerDelegate methods
//
// streamerStatusDidChange:
//
// Invoked when the AudioStreamer
// reports that its playback status has changed.
//
- (void)streamerStatusDidChange:(AudioStreamer *)sender {
	if ([sender isWaiting]) {
		NSLog(@"Stream Loading...");
		[self.statusItem setImage:[NSImage imageNamed:@"loading"]];
	}
	else if ([sender isPlaying]) {
		NSLog(@"Stream is Playing...");
		[self.statusItem setImage:[NSImage imageNamed:@"playing"]];

		[stopItem setEnabled:YES];
		[self.audioStreamer setVolume:currentVolume];
	}
	else if ([sender isDone]) {
		NSLog(@"Stream Closed");
		[self.statusItem setImage:[NSImage imageNamed:@"normal"]];
		[self stop];
	}
}


//
// updateProgress:
//
// Invoked when the AudioStreamer
// reports that its playback progress has changed.
//
- (void)updateProgress:(NSTimer *)updatedTimer {
	NSString* currentSong = [self.audioStreamer currentSong];
	if (currentSong == nil || self.statusItem.toolTip == currentSong)
		return;

	NSLog(@"Song changed: %@", currentSong);

	// Change song title
	[self.statusItem setToolTip:currentSong];
}

@end
