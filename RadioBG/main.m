//
//  main.m
//  RadioBG
//
//  Created by Dimitar Dimitrov on 3/11/15.
//  Copyright (c) 2015 MMmedia. All rights reserved.
//

#import "AppDelegate.h"
#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {

	AppDelegate * delegate = [[AppDelegate alloc] init];
	[[NSApplication sharedApplication] setDelegate:delegate];
	[NSApp run];

	return NSApplicationMain(argc, (const char **)argv);
}
