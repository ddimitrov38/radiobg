//
//  AppDelegate.h
//  RadioBG
//
//  Created by Dimitar Dimitrov on 3/11/15.
//  Copyright (c) 2015 MMmedia. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AudioStreamer.h"


@interface AppDelegate : NSObject <NSApplicationDelegate, AudioStreamerDelegate> {
	NSTimer *progressUpdateTimer;
}

@property (strong, nonatomic) NSMenu *statusMenu;
@property (strong, nonatomic) NSStatusItem *statusItem;
@property (strong, nonatomic) AudioStreamer *audioStreamer;

@end

